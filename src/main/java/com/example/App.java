package com.example;

import java.util.Arrays;

public class App {
    
    public static void main(String[] args) {
        int[] numbers = new int[args.length];
        
        // Parse command-line arguments to integers
        for (int i = 0; i < args.length; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }
        
        // Sort the numbers
        Sorting.sort(numbers);
        
        // Print the sorted numbers
        System.out.println("Sorted numbers:");
        for (int number : numbers) {
            System.out.println(number);
        }
    }

}
