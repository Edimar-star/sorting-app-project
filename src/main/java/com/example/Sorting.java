package com.example;

public class Sorting {
    public static void sort(int[] array) {
        if (array == null) throw new IllegalArgumentException();
        if (array.length <= 1) return;

        int aux = 0;
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    aux = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = aux;
                }
            }
        }
    }
}
