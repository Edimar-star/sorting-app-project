package com.example;

import org.junit.Test;
import java.util.Arrays;
import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.assertEquals;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SortingTest {

    private int value;
    private int expected;

    public SortingTest(int value, int expected) {
        this.value = value;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> data() {
        int[] array = {3, 4, 2, 0, 1, 5, 9, 7, 8, 6};
        int[] values = array.clone();
        int[] expectedValues = array.clone();

        Sorting.sort(values);
        Arrays.sort(expectedValues);

        Object[][] matriz = new Object[array.length][2];
        for(int i = 0; i < array.length; i++) {
            matriz[i][0] = values[i];
            matriz[i][1] = expectedValues[i];
        }

        return Arrays.asList(matriz);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testNullCase() {
        Sorting.sort(null);
    }
    
    @Test
    public void testGeneralCase() {
        assertEquals(value, expected);
    }

}
